
<?php 
	session_start();
	
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/aos.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="stylesheet" type="text/css" href="css/animate.compat.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="index_corps ">
<?php if (isset($_SESSION['USER'])) {

	header ('location : header_account.php');
	 }else{ ?>

		<div class="container-fluid ">
			<div class="row">
				<div class=" menu navbar navbar-default entete  col-md-12 col-sm-12  col-xm-12">
					<h2>AMAZONES</h2>
				</div>
			</div>
			<div  class="row">
				<div class="col-md-12">	
					<h1>Rejoignez nous</h1>			
				</div>
				<div class="col-md-10">
					<form>
						<div class="col-md-2  bt1">
							<a href="pages/inscription.php" class="btn btn-block btn-info btn-success bt1" style="background: #cddc39;">Inscription</a>
							
						</div>
						<div class="col-md-2 bt2">
							<a href="pages/connexion.php" class="btn btn-block btn-info btn-success " style="background: #cddc39">Connexion</a>
							
						</div>
						
						
					</form>
					
				</div>
				
			</div>
		</div>
		
	<?php  } ?>

	

</body>
</html>