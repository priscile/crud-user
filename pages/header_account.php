<?php session_start(); ?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">
<?php if (isset($_SESSION['USER'])) {?>
	<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<ul class="nav navbar-nav navbar-right">       
						<li class="dropdown">         
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" > <span class="user_name"><?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom']; ?></span><?php echo '<img src="../images/'.$_SESSION['USER']['photo'].'" style="width:60px; height:60px; border-radius: 50%; ">' ?> </a>    
							 <ul class="dropdown-menu">           
							 	<li><a href="profil.php"><span class="glyphicon glyphicon-user"></span>Profil</a></li>           
							 	<li><a href="deconnexion.php"><span class="glyphicon glyphicon-lock"></span>Deconnexion</a></li>        
							 </ul>       
						</li>     
					</ul>

			 	</div>
			</nav>
			<?php  } else {
			header('location:header_account.php');
			}?>
			
		
	


	<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

</body>
</html>