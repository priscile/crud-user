<?php session_start()?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style2.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<!-- <link rel="stylesheet" href="../bootstrap_4/css/bootstrap.min.css"> -->
	
	
</head>

<body >
	<?php

    if (isset($_POST['pwd']) AND isset($_POST['email'])) {
        $bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
        $response=$bdd->query('SELECT * FROM utilisateur ');
        $i=1;
        $allUser=array();
        while ($donnees =$response->fetch()) {
            $allUser[$i]=$donnees;
            $i++; 

        }
        $total=$i;
        for ($i=0; $i <$total ; $i++) { 
            if ($allUser[$i]['email']==$_POST['email'] AND $allUser[$i]['pwd']==$_POST['pwd']) {
                if ( $allUser[$i]['niveau']==5) {
                    $rep=2;
                    $res=0;
                    break;
                }
                   
                }else{
                   $res=1;
                }      
        }
        if($res==0 AND $rep==2){
        	 $_SESSION['ADMIN']=$allUser[$i];
            header('location: traitement_admin.php');
        }else{
            header('location: connexion.php');
            $_SESSION['mess']="email ou mot de passe inexistant";
        }
    }

  ?>
<?php  // print_r($_SESSION); ?>



<div class="container-fluid" style="">
	<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<ul class="nav navbar-nav navbar-right">       
						<li class="dropdown">         
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" > <span class="user_name"><?php echo $_SESSION['ADMIN']['nom']." ".$_SESSION['ADMIN']['prenom']; ?></span><?php echo '<img src="../images/'.$_SESSION['ADMIN']['photo'].'" style="width:60px; height:60px; border-radius: 50%; ">' ?> </a>        
							 <ul class="dropdown-menu">           
							 	<li><a href="profil.php"><span class="glyphicon glyphicon-user"></span>Profil</a></li>           
							 	<li><a href="deconnexion.php"><span class="glyphicon glyphicon-lock"></span>Deconnexion</a></li>        
							 </ul>       
						</li>     
					</ul>

			 	</div>
		</nav>


			<table class="table table-bordered">
				<thead class="thead-light">
					<tr style="background-color: pink;">
						<th class="text-center id"style="width: 30px;"> id </th>
						<th class="text-center id"style="width: 30px;"> photo </th>
						<th class="text-center nom"style="width: 250px;"> Nom </th>
						<th class="text-center prenom" style="width: 250px;"> Prenom </th>
						<th class="text-center prenom" style="width: 250px;"> Email </th>
						<th class="text-center etat" style="width: 80px;"> Etat </th>
						<th class="text-center action" style="width: 200px;"> Action </th>
					</tr>
				</thead>
				<tbody>
					<?php
					$bdd = new PDO('mysql:host=localhost;dbname=users', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
					$response = $bdd->query('SELECT * FROM utilisateur WHERE niveau!=5');
					$i = 1;
					$user = array();
					while ($donnees = $response->fetch()) {
						$user[$i] = $donnees; ?>
						<tr>
							<th ><?php echo $i; ?></th>
							<td> <img src="../images/<?php echo $donnees['photo'];?>" style=" width: 80px; height: 80px; border-radius: 50%;"></td>
							<td style="text-align:center;"><?php echo $donnees['nom']; ?></td> 
							<td style="text-align:center;"><?php echo $donnees['prenom']; ?></td> 
							<td style="text-align:center;"><?php echo $donnees['email']; ?></td> 
							<td style="text-align:center;">
								<?php if ($donnees['niveau'] == 1) { ?>
									<span style='color:green;font-weight:800;'> actif <span>
									<?php } else if ($donnees['niveau'] == 2) {  ?>
										<span > inactif <span>
										<?php } else { ?>
											<span > supprimer <span>
											<?php  } ?>
										</td>
										<td> 
											<div style="font-size: 1px;"> 
												<form enctype="multipart/form-data" method="post" action="action.php" style=" display: inline-block;">
													<a href="profil.php" >
														<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
														<input type="hidden" name="N2"  value="editer">
														<button  type="submit" style="background-color:inherit; border: none;">
															<span class=" des fa fa-pencil" style="color:blue; font-size: 12px;"> editer 
															</span>
														</button>
													</a>
												</form>
												<?php if($donnees['niveau'] <=2 ){ ?>
													<form enctype="multipart/form-data" method="post" action="action.php" style=" display: inline-block;">
														<a href="profil_consulte.php" >
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="supprimer">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class="  fa fa-trash Supprimer" style="margin-left: 10px;color:red; font-size: 12px;"> supprimer </span>
															</button>
														</a>
													</form>
												<?php 	} 
												if ($donnees['niveau'] == 1) { ?>
													<form enctype="multipart/form-data" method="post" action="action.php" style=" display: inline-block;">
														<a href=""  >
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="desactiver">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class="  glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 12px;"> desactiver </span>
															</button>
														</a>
													</form>
												<?php	} else { ?>
													<form enctype="multipart/form-data" method="post" action="action.php" style=" display: inline-block;">
														<a href="#">
															<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
															<input type="hidden" name="N2"  value="activer">
															<button  type="submit" style="background-color:inherit; border: none;">
																<span class=" des3 glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 12px;">activer </span>
															</button>
														</a>
													</form>
												</div>
											</td>
										</tr>
									<?php	}
									$i++;
								}
								?>
							</tbody>
						</table>
					</div>
					<script type="text/javascript" src="../javascript/jquery.min.js"></script>
					<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
					<script type="text/javascript">

						var form1 = document.getElementsByClassName('form11');
						var form2 = document.getElementsByClassName('form2');
						var form3 = document.getElementsByClassName('form3');
						var form4 = document.getElementsByClassName('form4');
						var edit = document.getElementsByClassName('editer');
						var supp = document.getElementsByClassName('Supprimer');
						var act = document.getElementsByClassName('activer');
						var act_inverse = document.getElementsByClassName('activer_inverse');


					</script>

				</div>
				<script type="text/javascript" src="../javascript/jquery-3.6.0.min.js"></script>
				<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>

</body>
</html> 



    
	