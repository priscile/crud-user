<?php session_start(); ?>

<?php  
    if( isset($_SESSION) && $_SESSION!=array() ){
        if(isset($_POST) || isset($_FILES)){
            // echo "<br>";
            // print_r($_FILES);
            //on se connecte a la BDD
            $bdd = new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
            // on recupere l'ID correspondant
            $demande=$bdd->prepare('SELECT id FROM utilisateur WHERE email = ?');
            $demande->execute(array($_SESSION['USER']['email']));
             $id = $demande->fetch();
            // print_r($_POST);
            if($_POST['nom']!=$_SESSION['USER']['nom']){
                $insertnom = $bdd->prepare("UPDATE utilisateur SET nom = ? WHERE id = ?");
                $insertnom->execute(array($_POST['nom'], $id[0]));
            }
            if($_POST['prenom']!=$_SESSION['USER']['prenom']){
                $insertprenom = $bdd->prepare("UPDATE utilisateur SET prenom = ? WHERE id = ?");
                $insertprenom->execute(array($_POST['prenom'], $id[0]));
            }
            if($_POST['email']!=$_SESSION['USER']['email']){
                $insertmail = $bdd->prepare("UPDATE utilisateur SET email = ? WHERE id = ?");
                $insertmail->execute(array($_POST['email'], $id[0]));
            }
            if (!empty($_POST['pwd'])) {
                if($_POST['pwd']!=$_SESSION['USER']['pwd']){
                    print_r($_POST['pwd']);
                    $insertpwd = $bdd->prepare("UPDATE utilisateur SET pwd = ? WHERE id = ?");
                    $insertpwd->execute(array($_POST['pwd'], $id[0]));
                }
            }
            if (isset($_FILES['photo']) AND $_FILES['photo']['error'] == 0){
                if($_FILES['photo']['name']!=$_SESSION['USER']['photo']){
                    if($_FILES['photo']['size'] <= 3000000){ // on verifie la taille
                        // on insere la valeur dans la bdd
                        $insertphoto = $bdd->prepare("UPDATE utilisateur SET photo = ? WHERE id = ?");
                        $insertphoto->execute(array($_FILES['photo']['name'], $id[0]));
                        // on stocke l'image dans le fichier adequat
                        move_uploaded_file($_FILES['photo']['tmp_name'], '../images/' . basename($_FILES['photo']['name']));

                    }
                    
                }
            }
            
            // on recupere les in formations et on actualise les valeurs de session
            $dataRecup=$bdd->prepare('SELECT * FROM utilisateur WHERE id = ?');
            $dataRecup->execute(array($id[0]));
             
                $_SESSION = array();
                $_SESSION['USER']=$dataRecup->fetch();
                // echo "ici la nouvelle session: ";
                // print_r($_SESSION);
            if (isset($_SESSION['admin']) && $_SESSION['admin']=='ok') {    // si l'access a ete fait depuis le compte admin.
                header('location:profil.php');  // on redirige vers la page de l'admin.
            }else{                            // si non.  
                header('location:profil.php');        // on redirige vers la page de l'utilisateur.
            }    
            
        }

    // }else{
        // header('location: user_account.php');
    }
?>


