<?php session_start()?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">
	
		<div class="container-fluid ">
			<div class="row">
				<div class=" menu navbar navbar-inverse entete  col-md-12 col-sm-12  col-xm-12">
					<ul class="nav navbar-nav collapse  navbar-collapse pull-right">
						<li><a class="btn btn-block" href="inscription.php">INSCRIPTION</a></li>
						<li><a class="" href="">/</a></li>
						<li><a class="btn btn-block " href="connexion.php">CONNECTION</a></li>
						<li><a class="btn btn-block " href="../index.php">ACCUEIL</a></li>
					</ul>
				</div>
			</div>
			
			<div class="row ">
				<h2></h2>
				<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10">
					<h3>INSCRIVEZ VOUS</h3>
					<form enctype="multipart/form-data" method="post" action="traitement.php" id="myform" >
						<p>   
							<?php 
	                        	if(isset($_SESSION['message_error']) ){
	                        		echo $_SESSION['message_error'];	
	                        	}
	                        
	                        ?>
	                         	
	                    </p>
						<div class="row">
							<div class="col-md-5 col-xm-5 col-xs-offset-1 col-xs-11 form2">
								<span class="glyphicon glyphicon-user"></span>
								<label>Noms</label>
								<input id="name" class=" form-control inpt1 " type="text" name="nom" placeholder="veuillez entrer votre nom"  required="">
								<p></p>
								<span class="glyphicon glyphicon-user"></span>
								<label>Prenoms</label>
								<input class="form-control inpt2" type="text" name="prenom" placeholder="veuillez entrer votre nom"  id="prenom" required="">
								<p></p>
								<span class="glyphicon glyphicon-envelope"></span>
								<label>email</label>
								<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
								<p></p>
								<span class="glyphicon glyphicon-lock"></span>
								<label>mot de passe</label>
								<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required="">
								<input type="hidden" name="niveau" value="1"><br>
							</div>
							<div class="col-md-offset-1 col-md-5 col-md-pull-1 col-xm-5 col-xs-offset-1 col-xs-11 form3" style="margin-top: 100px;">	
								<span class="glyphicon glyphicon-picture"></span>
								<label>Telecharger votre photo de pofil</label>
								<input class="form-control inpt7" type="file" name="photo" accept="image/*" onchange="loadFile(event)" required="" >
								<div class="col-md-offset-4 col-md-4 im" style="">
		                        	<img id="im">
		                  		</div>
							</div>
						</div>
						<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 ">
							<input  type="reset"  class="  btn btn-block btn-info bout1"> 
							
						</div>
						<div class="  col-md-4 col-xm-5 col-xs-6">
							<input  type="submit"  class="  btn btn-block btn-info btn-success bout2" >
						</div>
						<!-- <div class="col-md-3">
							<input  type="submit" name="enregistrer" class=" bout btn btn-block btn-info" >
						</div> -->
							
					</form>		
				</div>
			</div>
			
		</div>	
	
	
</body>
	<?php         
		// unset($_['message_error']);
		session_destroy();
	?>
<script type="text/javascript">
    var loadFile = function(event) {
    var profil = document.getElementById('im');
    profil.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
</html>
	