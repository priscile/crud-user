<?php session_start()?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
</head>
<body class="corps">

	<div class="container-fluid">
		<div class="row">
			<div class=" menu navbar navbar-inverse entete  col-md-12 col-sm-12  col-xm-12">
				<ul class="nav navbar-nav collapse  navbar-collapse pull-right">
					<li><a class="" href="inscription.php">INSCRIPTION</a></li>
					<li><a class="" href="">/</a></li>
					<li><a class="" href="connexion.php">CONNEXION</a></li>
					<li><a class="" href="../index.php">ACCUEIL</a></li>
				</ul>
			</div>
		</div>
		
		<div class="row ">
			<h2></h2>
			<div class=" form1 col-md-offset-2 col-md-8  col-xs-12 col-xm-offset-2 col-xm-10">
				<h3>CONNECTEZ-VOUS</h3>
				<form enctype="multipart/form-data" method="post" action="traitement_admin.php" id="myform" >
					<p style="text-align: center;">   
						<?php 
                        	if(isset($_SESSION['mess'])){
                         		echo $_SESSION['mess'];
                         	}

                        ?>
                         	
                    </p>
					<div class=" col-md-offset-3 col-md-6" style="margin-top: 60px;">
						<span class="glyphicon glyphicon-envelope"></span>
						<label>email</label>
						<input class="form-control inpt3" type="email" name="email" placeholder="veuillez entrer votre adresse mail" id="mail" required="">
						<p></p>
						
						<span class="glyphicon glyphicon-lock"></span>
						<label style="margin-top: 50px;">mot de passe</label>
						<input class="form-control inpt4" type="password" name="pwd" placeholder="Veuillez entrer votre mot de passe" id="pwd"  required=""><br>
						
					</div>
			
					<div class="col-md-offset-2 col-md-4 col-xm-5 col-xs-6 ">
						<input  type="reset"  class="  btn btn-block btn-info bout1"> 
						
					</div>
					<div class="  col-md-4 col-xm-5 col-xs-6">
						<input  type="submit"  class="  btn btn-block btn-info btn-success bout2" >
					</div>
											
				</form>		
			</div>
		</div>
		
	</div>


</body>
<?php
	session_destroy();
?>



</html>